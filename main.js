// 1. Ми можемо стоврити новий HTML елемент за допомогою методу "document.createElement();"
// 2. "insertAdjacentHTML" визначає, де саме вставити HTML, який передається як другий параметр функції.
//    Можливі варіанти: "beforbegin", "afterbegin", "beforeend", "afterend",
// 3. Для видалення елемента зі сторінки можна використати метод "remove()".

function displayList(arr, parent = document.body) {
    const list = document.createElement("ul");
    parent.appendChild(list);
    
    arr.forEach((item) => {
      const listItem = document.createElement("li");
      
      if (Array.isArray(item)) {
        displayList(item, listItem);
      } else {
        const text = document.createTextNode(item);
        listItem.appendChild(text);
      }
      
      list.appendChild(listItem);

      setTimeout(() => {
        document.body.innerHTML = '';
    }, 3000);
    });
  }

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", ["Borispol", "Irpin"]];
displayList(arr)


// Таймер
let secondsLeft = 3;

function showTimer() {
  const timerElement = document.getElementById('timer');
  timerElement.textContent = secondsLeft + ' seconds left';
  secondsLeft--;
}

const timerInterval = setInterval(showTimer, 1000);

// Очищаємо сторінку через 3 секунди
setTimeout(() => {
  clearInterval(timerInterval); 
  document.body.innerHTML = '';
}, 3000);





